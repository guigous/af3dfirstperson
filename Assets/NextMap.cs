using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextMap : MonoBehaviour
{
    MeshRenderer mesh;

    
    // Start is called before the first frame update
    void Start()
    {
        mesh=GetComponent<MeshRenderer>();
        mesh.enabled = false;
        

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            SceneManager.LoadScene("Level02");
        }
    }
}
